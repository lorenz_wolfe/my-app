const initialState = {
  email: '',
  password: '',
  error: '',
  user: '',
  loading: false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'CLEAR_ERROR_AND_SET_LOADING':
      state = {
        ...state,
        error: action.payload.error,
        loading: action.payload.loading
      };
      break;
      case 'SET_ERROR_SET_LOADING_SET_USER':
        state = {
          ...state,
          error: action.payload.error,
          loading: action.payload.loading,
          user: action.payload.user
        };
        break;
      case 'SET_EMAIL_CLEAR_PASSWORD_SET_ERROR_SET_LOADING_SET_USER':
      state = {
        ...state,
        email: action.payload.email,
        password: action.payload.password,
        loading: action.payload.loading,
        error: action.payload.error,
        user: action.payload.user
      };
      break;
    case 'SET_EMAIL_VALUE':
      state = {
        ...state,
        email: action.payload.email
      };
      break;
      case 'SET_PASSWORD_VALUE':
        state = {
          ...state,
          password: action.payload.password
        };
        break;
  }
  return state;
};

export default reducer;
