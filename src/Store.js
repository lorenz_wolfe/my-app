import {createStore} from "redux";
import reducer from "./reducers/SessionReducer";

const Store = createStore(reducer);
Store.subscribe(() => {
    console.log('Store changed ', Store.getState());
});
export default Store;
