import React, {Component} from 'react';
import firebase from 'firebase';
import {connect} from 'react-redux';
import Header from './components/header';

class Landing extends Component {

  render() {
if(firebase.auth().currentUser) {
  firebase.auth().currentUser.getIdToken(/* forceRefresh */
true).then(function(idToken) {
  console.log('logged in');
}).catch(function(error) {
  console.log('not logged in');
});} else {
  console.log('no current user');
}


    const isLoggedIn = this.props.user;
    const landing = isLoggedIn ? (
      <p>
        Logged in Landing Page
      </p>
    ) : (
      <p>
        Not Logged in Landing Page
      </p>
    );
    return (<div className="row">
      <Header/>
      {landing}

    </div>);
  }
}
function mapStateToProps(state) {
  return {user: state.user, email: state.email, error: state.error, loading: state.loading};
}
export default connect(mapStateToProps)(Landing);
