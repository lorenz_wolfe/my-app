import React, {Component} from 'react';

class HandleError extends Component {
  render() {
    if (this.props.error) {
      return (<div className="card-panel red lighten-2">{this.props.error}</div>);
    } else {
      return <p></p>
    }
  }
}
export default HandleError;
