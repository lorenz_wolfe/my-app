import React, {Component} from 'react';

export default class header extends Component {
  render(){
    return (
      <div>
        <nav>
          <div className="nav-wrapper">
            <a href="#" className="brand-logo">Logo</a>
            <ul id="nav-mobile" className="right hide-on-med-and-down">
              <li>
                <a href="sass.html">Sass</a>
              </li>
              <li>
                <a href="badges.html">Components</a>
                </li>
              <li>
                <a >Login</a>
              </li>
            </ul>
          </div>
        </nav>
        </div>
    );
  }
}
