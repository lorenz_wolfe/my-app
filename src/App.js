import React, {Component} from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import './App.css';
import firebase from 'firebase';
import Login from './Login';
import Landing from './Landing';

class App extends Component {
  state = {
    loggedIn: null
  };

  componentWillMount() {
    firebase.initializeApp({
      apiKey: "AIzaSyBybPurVEtiz0UKJfHXHmfbhP7L3NsWrfA",
      authDomain: "authenticate-af3fb.firebaseapp.com",
      databaseURL: "https://authenticate-af3fb.firebaseio.com",
      projectId: "authenticate-af3fb",
      storageBucket: "authenticate-af3fb.appspot.com",
      messagingSenderId: "461426256612"
    });

    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({loggedIn: true});
      } else {
        this.setState({loggedIn: false});
      }
    });
  }
  render() {
    return (<div className="App">
      <BrowserRouter>
        <div>
          <Route exact={true} path="/" component={Login}/>
          <Route path="/landing" component={Landing}/>
        </div>
      </BrowserRouter>
    </div>);
  }
}

export default App;
