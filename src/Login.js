import React, {Component} from 'react';
import {connect} from 'react-redux';
import 'materialize-css/dist/css/materialize.css';
import firebase from 'firebase';
import Header from './components/header';
import HandleError from './components/HandleError';

class Login extends Component {
  constructor(props) {
    super(props);
    this.handleInput = this.handleInput.bind(this);
    this.onButtonPress = this.onButtonPress.bind(this);
    this.state = {
      email: '',
      password: '',
      error: '',
      loading: false
    };
  }
  onButtonPress(event) {
    event.preventDefault();
    // this.setState({error: '', loading: true});
    this.props.dispatch({type: 'CLEAR_ERROR_AND_SET_LOADING', payload: {
        error: '',
        loading: true
      }});

    firebase.auth().signInWithEmailAndPassword(this.props.email, this.props.password).then(this.onLoginSuccess.bind(this)).catch(() => {
      firebase.auth().createUserWithEmailAndPassword(this.props.email, this.props.password).then(this.onLoginSuccess.bind(this)).catch(this.onLoginFail.bind(this));
    });
  }
  onLoginFail(data) {
    //this.setState({error: 'Authentication Failed.', loading: false, user: ''});
    this.props.dispatch({type: 'SET_ERROR_SET_LOADING_SET_USER', payload: {
        error: 'Authentication Failed.',
        loading: false,
        user: null
      }});
  }
  onLoginSuccess(data) {
    //this.setState({email: '', password: '', loading: false, error: '', user: data.users})
    this.props.dispatch({type: 'SET_EMAIL_CLEAR_PASSWORD_SET_ERROR_SET_LOADING_SET_USER', payload: {
        password: '',
        email: '',
        error: '',
        loading: false,
        user: data
      }});
      this.props.history.push("/landing");
  }

  handleInput(event) {
    let value = event.target.value;
    // this.setState({
    //   [event.target.id]: value
    // });
    let payload = {
        [event.target.id]: value
      };
    if(event.target.id === 'email'){
      this.props.dispatch({type: 'SET_EMAIL_VALUE', payload:payload});
    } else {
      this.props.dispatch({type: 'SET_PASSWORD_VALUE', payload:payload});
    }

  }

  render() {
    return (<div className="row">
      <Header user={this.props.user}/>
      <form className="col s12">
        <HandleError error={this.props.error}/>
        <div className="row">
          <div className="row"></div>
          <span className="col s2"/>
          <div className="input-field col s4">
            <input placeholder="email" onChange={this.handleInput} id="email" type="text" value={this.props.username}/>
            <label ></label>
          </div>
          <div className="input-field col s4">
            <input id="password" placeholder="password" onChange={this.handleInput} type="password" value={this.props.password}/>
            <label ></label>
            <span className="col s2"/>
          </div>
          <div className="col s12">
            <button className=" btn waves-effect waves-light" onClick={this.onButtonPress} type="submit" name="action">Submit
            </button>
          </div>

        </div>
      </form>
    </div>);
  }
}
function mapStateToProps(state) {
  return {
    email: state.email,
    password: state.password,
    error: state.error,
    loading: state.loading
  };
}
export default connect(mapStateToProps)(Login);
